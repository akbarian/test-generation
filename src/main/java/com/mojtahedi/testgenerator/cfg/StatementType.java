package com.mojtahedi.testgenerator.cfg;

public enum StatementType {
    IF , FOR, SWITCH
}
