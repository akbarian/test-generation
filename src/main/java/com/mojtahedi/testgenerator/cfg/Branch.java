package com.mojtahedi.testgenerator.cfg;

import java.util.ArrayList;
import java.util.List;

public class Branch {

    private String condition;
    private int weight;
    private int trace;
    private StatementType statementType;
    private Branch parent;
    private List<Branch> children = new ArrayList<>();
    private int depth;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getTrace() {
        return trace;
    }

    public void setTrace(int trace) {
        this.trace = trace;
    }

    public Branch getParent() {
        return parent;
    }

    public void setParent(Branch parent) {
        this.parent = parent;
    }

    public List<Branch> getChildren() {
        return children;
    }

    public void setChildren(List<Branch> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "{\"Branch\":{"
                + "\"condition\":\"" + condition + "\""
                + ", \"weight\":\"" + weight + "\""
                + ", \"trace\":\"" + trace + "\""
                + ", \"statementType\":\"" + statementType + "\""
                + ", \"depth\":\"" + depth + "\""
                + ", \"children\":" + children
                + "}}";
    }

    public StatementType getStatementType() {
        return statementType;
    }

    public void setStatementType(StatementType statementType) {
        this.statementType = statementType;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }
}
