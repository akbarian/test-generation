/**
 * @(#) Target.java    v. 1.0 - March 20, 2004
 * <p>
 * This software was written by Paolo Tonella (tonella@itc.it) at ITC-irst,
 * Centro per la Ricerca Scientifica e Tecnlogica.
 * <p>
 * Distributed under the Gnu GPL (General Public License). See GPL.TXT
 */

package com.mojtahedi.testgenerator;

import java.util.LinkedList;
import java.util.List;

abstract class Target {
    /**
     * For targets with sub-targets (e.g., methods having branches as
     * sub-targets). By default, it returns the current target.
     */
    public List getSubTargets() {
        List subTargets = new LinkedList();
        subTargets.add(this);
        return subTargets;
    }

    /**
     * At least one individual in population covers target.
     */
    public abstract boolean coveredBy(Chromosome id);

}
