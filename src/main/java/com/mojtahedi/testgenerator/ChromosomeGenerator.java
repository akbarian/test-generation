package com.mojtahedi.testgenerator;

import com.github.javaparser.JavaParser;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ForStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.mojtahedi.testgenerator.cfg.Branch;
import com.mojtahedi.testgenerator.cfg.StatementType;
import com.mojtahedi.testgenerator.example.ClassUnderTheTest;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//import openjava.mop.MOPException;

public class ChromosomeGenerator {
    static JavaParser javaParser;

    private List<Branch> rootBranches;

    private static int finalTrace = 1;

    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {

        String javaPathName = "E:\\Development\\Projects\\first-project\\src\\main\\java\\";

        ChromosomeFormer chromosomeFormer = new ChromosomeFormer();

        addConstructors(ClassUnderTheTest.class, ClassUnderTheTest.class.getConstructors(), chromosomeFormer);
        addMethods(ClassUnderTheTest.class, chromosomeFormer);

        chromosomeFormer.setClassUnderTest(ClassUnderTheTest.class.getName());
        for (int j = 0; j < Population.populationSize; j++) {
            chromosomeFormer.buildNewChromosome();
            System.out.println(chromosomeFormer.getChromosome());
        }


        CompilationUnit cu = StaticJavaParser.parse(new File(
                javaPathName + "com\\mojtahedi\\testgenerator\\example\\ClassUnderTheTest.java"
        ));


        cu.findAll(ClassOrInterfaceDeclaration.class).stream()
                .forEach(c -> {
                    VariableDeclarator variables = new VariableDeclarator();
                    variables.setName("traces");
                    variables.setType(new ClassOrInterfaceType("public static final java.util.List<Integer>"));
                    variables.setInitializer("new java.util.ArrayList<>()");
                    FieldDeclaration fieldDeclaration = new FieldDeclaration().addVariable(variables);
                    c.getMembers().add(0, fieldDeclaration);
                });

        //System.out.println(coid);
        int counter = 0;
        for (int i = 0; i < cu.findAll(Statement.class).size(); i++) {
            Statement statement = cu.findAll(Statement.class).get(i);

            if (statement.getParentNode().isPresent() && statement.getParentNode().get() instanceof MethodDeclaration) {

                Branch branch = extractBranchesFromStatement(null, null, statement , 1 );
                System.out.println(branch);
            }

            if (statement.isBlockStmt()) {

                Expression insertTraceStatement = StaticJavaParser
                        .parseExpression("traces.add(" + (counter++) + ")");
                ((BlockStmt) statement).addStatement(0, insertTraceStatement);
            }
        }

        cu.findAll(ClassOrInterfaceDeclaration.class).stream()
                .forEach(c -> {
                    String oldName = c.getNameAsString();
                    String newName = "Manipulated" + oldName;
                    System.out.println("Renaming class " + oldName + " into " + newName);
                    c.setName(newName);
                });
        cu.findAll(ConstructorDeclaration.class).stream()
                .forEach(c -> {
                    String oldName = c.getNameAsString();
                    String newName = "Manipulated" + oldName;
                    System.out.println("Renaming class " + oldName + " into " + newName);
                    c.setName(newName);
                });


        File sourceRoot = new File(javaPathName);
        File sourceFile = new File(javaPathName + "com\\mojtahedi\\testgenerator\\example\\ManipulatedClassUnderTheTest.java");
        Files.write(sourceFile.toPath(), cu.toString().getBytes());

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        compiler.run(null, null, null, sourceFile.getPath());

        URLClassLoader classLoader = URLClassLoader.newInstance(new URL[]{sourceRoot.toURI().toURL()});
        Class<?> cls = Class.forName("com.mojtahedi.testgenerator.example.ManipulatedClassUnderTheTest", true, classLoader); // Should print "hello".
        Object instance = cls.newInstance(); // Should print "world".
        System.out.println(instance); // Should print "test.Test@hashcode".
    }

    private static Branch extractBranchesFromStatement(Branch parent, List<Branch> childBranches, Statement statement , int depth) {
        if (statement.isBlockStmt()) {
            Branch branch = new Branch();
            branch.setParent(parent);
            branch.setDepth(depth++);
            branch.setTrace(finalTrace++);
            if (statement.getParentNode().isPresent() && statement.getParentNode().get() instanceof Statement) {
                statement.getParentNode().ifPresent(node -> {
                    Statement nodeParent = (Statement) node;
                    if (nodeParent.isIfStmt()) {
                        String condition = ((IfStmt) nodeParent).getCondition().toString();
                        if (statement.equals(((IfStmt) nodeParent).getThenStmt())) {
                            branch.setCondition(condition);
                        } else {
                            branch.setCondition("!(" + condition + ")");
                        }
                        branch.setStatementType(StatementType.IF);

                    } else if (((Statement) node).isForStmt()) {
                        branch.setCondition(((ForStmt) node).getCompare().get().toString());
                        branch.setStatementType(StatementType.FOR);
                    }

                });
            }
            List<Branch> children = new ArrayList<>();
            if (!statement.getChildNodes().isEmpty()) {
                int finalDepth = depth;
                statement.getChildNodes().forEach(childNode -> {
                    if (childNode instanceof Statement) {
                        extractBranchesFromStatement(branch, children, (Statement) childNode , finalDepth);
                    }
                });
            }
            branch.setChildren(children);
            return branch;
        } else { // just for filling the children
            if (!statement.getChildNodes().isEmpty()) {
                for (int i = 0; i < statement.getChildNodes().size(); i++) {
                    Node childNode = statement.getChildNodes().get(i);
                    if (childNode instanceof Statement) {
                        Branch branch = extractBranchesFromStatement(parent, childBranches, (Statement) childNode , depth);
                        if (branch != null)
                            childBranches.add(branch);
                    }
                }
            }
        }
        return null;
    }

    private static String getMethodName(Node node) {
        String methodName = null;
        if (node instanceof MethodDeclaration) {

            methodName = ((MethodDeclaration) node).getNameAsString();
        } else if (node instanceof ConstructorDeclaration) {

            methodName = ((ConstructorDeclaration) node).getNameAsString();
        } else {
            if (node.getParentNode().isPresent()) {
                methodName = getMethodName(node.getParentNode().get());
            } else {
                return null;
            }
        }
        return methodName;
    }

    private static void addConstructors(Class clazz, Constructor<?>[] constructors, ChromosomeFormer chromosomeFormer) {
        for (int i = 0; i < constructors.length; i++) {
            Constructor<?> constructor = constructors[i];
            java.lang.reflect.Parameter[] parameters = constructor.getParameters();
            for (int j = 0; j < parameters.length; j++) {
                if (!isPrimitive(parameters[j])) {

                    addConstructors(parameters[j].getType(),
                            parameters[j].getType().getConstructors()
                            , chromosomeFormer);
                }
            }
            if (parameters.length != 0) { //skip string parameters
                MethodSignature methodSignature = new MethodSignature(constructor.getDeclaringClass().getName(),
                        Stream.of(parameters).map(parameter -> getParameterName(parameter.getType().getName())).collect(Collectors.toList()));
                chromosomeFormer.addConstructor(methodSignature);
            }

        }

        if (chromosomeFormer.getConstructors().size() == 0) {
            MethodSignature methodSignature = new MethodSignature(clazz.getName(), Collections.EMPTY_LIST);
            chromosomeFormer.addConstructor(methodSignature);
        }
    }

    private static void addMethods(Class<?> clazz, ChromosomeFormer chromosomeFormer) {
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (int i = 0; i < declaredMethods.length; i++) {
            java.lang.reflect.Parameter[] parameters = declaredMethods[i].getParameters();
            for (int j = 0; j < parameters.length; j++) {
                if (!isPrimitive(parameters[j])) {
                    addConstructors(parameters[j].getType(),
                            parameters[j].getType().getConstructors()
                            , chromosomeFormer);
                }
            }
            MethodSignature methodSignature = new MethodSignature(declaredMethods[i].getName(), Stream.of(parameters).map(parameter -> getParameterName(parameter.getType().getName())).collect(Collectors.toList()));

            chromosomeFormer.addMethod(ClassUnderTheTest.class.getName(), methodSignature);
        }
    }

    private static String getParameterName(String name) {
        if (name.equals("java.lang.String"))
            return "String";
        else
            return name;
    }

    private static boolean isPrimitive(java.lang.reflect.Parameter parameter) {
        if (parameter.getType().equals(Long.TYPE) ||
                parameter.getType().equals(Integer.TYPE) ||
                parameter.getType().equals(Float.TYPE) ||
                parameter.getType().equals(Double.TYPE) ||
                parameter.getType().equals(String.class) ||
                parameter.getType().equals(Boolean.TYPE)
        ) {
            return true;
        } else {
            return false;
        }
    }
}
