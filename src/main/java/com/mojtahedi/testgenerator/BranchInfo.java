/**
 * Copyright (C) 2010-2018 Gordon Fraser, Andrea Arcuri and EvoSuite
 * contributors
 *
 * This file is part of EvoSuite.
 *
 * EvoSuite is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3.0 of the License, or
 * (at your option) any later version.
 *
 * EvoSuite is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with EvoSuite. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mojtahedi.testgenerator;

import java.io.Serializable;

public class BranchInfo implements Serializable {

	private static final long serialVersionUID = -2145547942894978737L;

	private String className;
	
	private String methodName;
	
	private int lineNo;
	
	private String expression;

	public BranchInfo() {
	}

	public BranchInfo(String className, String methodName, int lineNo, String expression) {
		this.className = className;
		this.methodName = methodName;
		this.lineNo = lineNo;
		this.expression = expression;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getClassName() {
		return className;
	}

	public BranchInfo setClassName(String className) {
		this.className = className;
		return this;
	}

	public String getMethodName() {
		return methodName;
	}

	public BranchInfo setMethodName(String methodName) {
		this.methodName = methodName;
		return this;
	}

	public int getLineNo() {
		return lineNo;
	}

	public BranchInfo setLineNo(int lineNo) {
		this.lineNo = lineNo;
		return this;
	}

	public String getExpression() {
		return expression;
	}

	public BranchInfo setExpression(String expression) {
		this.expression = expression;
		return this;
	}
}
