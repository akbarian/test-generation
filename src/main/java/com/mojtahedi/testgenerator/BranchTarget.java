/**
 * @(#) BranchTarget.java    v. 1.0 - September 14, 2004
 * <p>
 * This software was written by Paolo Tonella (tonella@itc.it) at ITC-irst,
 * Centro per la Ricerca Scientifica e Tecnlogica.
 * <p>
 * Distributed under the Gnu GPL (General Public License). See GPL.TXT
 */

package com.mojtahedi.testgenerator;

class BranchTarget extends Target {
    /**
     * Branch to be covered.
     */
    int branch;

    /**
     * Branch to be covered is the only parameter.
     */
    public BranchTarget(int br) {
        branch = br;
    }

    /**
     * Used in Map's.
     */
    public int hashCode() {
        return branch;
    }

    /**
     * Used in Map's.
     */
    public boolean equals(Object obj) {
        BranchTarget tgt = (BranchTarget) obj;
        return branch == tgt.branch;
    }

    /**
     * Used in print's.
     */
    public String toString() {
        return Integer.toString(branch);
    }

    /**
     * At least one individual in population covers branch.
     */
    public boolean coveredBy(Chromosome id) {
        return id.coversBranch(this);
    }

}
